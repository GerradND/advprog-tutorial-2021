package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjediTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;
    private String dummyHolderName;
    private Heatbearer dummyHeatbearer;
    private TheWindjedi dummyTheWindjedi;
    private SpellbookAdapter dummySpellBookAdapter1;
    private SpellbookAdapter dummySpellBookAdapter2;

    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");
        dummyHolderName = "dummy";
        dummyHeatbearer = new Heatbearer(dummyHolderName);
        dummySpellBookAdapter1 = new SpellbookAdapter(dummyHeatbearer);
        dummyTheWindjedi = new TheWindjedi(dummyHolderName);
        dummySpellBookAdapter2 = new SpellbookAdapter(dummyTheWindjedi);
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    public void testGetHolderMethodReturnHolderName() throws Exception {
        assertEquals(dummyHolderName, dummySpellBookAdapter1.getHolderName());
        assertEquals(dummyHolderName, dummySpellBookAdapter2.getHolderName());
    }

    @Test
    public void testNormalAttackHeatbearerSpellBookAdapterReturnSameText() throws Exception {
        String expectedStr = "Enemy scarred";
        String dummyNormalAttack = dummySpellBookAdapter1.normalAttack();
        assertEquals(expectedStr, dummyNormalAttack);
    }
    @Test
    public void testNormalAttackTheWindJediSpellBookAdapterReturnSameText() throws Exception {
        String expectedStr = "Small musical attack launched";
        String dummyNormalAttack = dummySpellBookAdapter2.normalAttack();
        assertEquals(expectedStr, dummyNormalAttack);
    }
    @Test
    public void testChargedAttackHeatbearerSpellBookAdapterReturnSameText1() throws Exception {
        String expectedStr = "EXPUULOOOOSHHHIOONNNN!";
        String dummyChargedAttack = dummySpellBookAdapter1.chargedAttack();
        assertEquals(expectedStr, dummyChargedAttack);
    }

    @Test
    public void testChargedAttackHeatbearerSpellBookAdapterReturnSameText2() throws Exception {
        String expectedStr = "can't use the spell. Recharging!";
        String dummyChargedAttack = dummySpellBookAdapter1.chargedAttack();
        dummyChargedAttack = dummySpellBookAdapter1.chargedAttack();
        assertEquals(expectedStr, dummyChargedAttack);
    }
    @Test
    public void testChargedAttackTheWindjediSpellBookAdapterReturnSameText1() throws Exception {
        String expectedStr = "Orchestra-class music attack launched";
        String dummyChargedAttack = dummySpellBookAdapter2.chargedAttack();
        assertEquals(expectedStr, dummyChargedAttack);
    }
}
