package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FacadeHelperTest {
    private Class<?> facadeHelperClass;

    @BeforeEach
    public void setup() throws Exception {
        facadeHelperClass = FacadeHelper.class;
    }

    @Test
    public void testFacadeHelperIsAPublicClass() {
        int classModifiers = facadeHelperClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testFacadeHelperHasEncodeMethod() throws Exception {
        Method encode = facadeHelperClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeHelperHasDecodeMethod() throws Exception {
        Method decode = facadeHelperClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
