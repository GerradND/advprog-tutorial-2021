package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.RegisTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public class FacadeHelper {
    private List<Transformation> list;

    public FacadeHelper(){
        list = new ArrayList<Transformation>();
        list.add(new CelestialTransformation());
        list.add(new AbyssalTransformation());
        list.add(new RegisTransformation());
    }

    public Spell encode(Spell spell){
        for(Transformation transformation : list){
            spell = transformation.encode(spell);
        }
        return spell;
    }

    public Spell decode(Spell spell){
        for(int i = list.size()-1; i > -1; i--){
            spell = list.get(i).decode(spell);
        }
        return spell;
    }
}
