package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private int counter = 0;

    public SpellbookAdapter(Spellbook spellbook) {
        if(spellbook.getName().equalsIgnoreCase("Heat Bearer")){
            this.spellbook = new Heatbearer(spellbook.getHolderName());
        }
        else{
            this.spellbook = new TheWindjedi(spellbook.getHolderName());
        }
    }

    @Override
    public String normalAttack() {
        counter = 0;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(counter < 1) {
            counter += 1;
            return spellbook.largeSpell();
        }
        else{
            counter = 0;
            return "can't use the spell. Recharging!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
