package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.AdapterInitializer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    private int counter = 0;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    AdapterInitializer adapterInitializer;

    @Override
    public List<Weapon> findAll() {
        List<Weapon> weaponList = weaponRepository.findAll();
        if(counter == 0){
            counter += 1;
            List<Bow> bowList = bowRepository.findAll();
            List<Spellbook> spellbookList = spellbookRepository.findAll();
            for (Bow bow : bowList) {
                BowAdapter bowAdapter = new BowAdapter(bow);
                weaponRepository.save(bowAdapter);
                weaponList.add(bowAdapter);
            }
            for (Spellbook spellbook : spellbookList) {
                SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbook);
                weaponRepository.save(spellbookAdapter);
                weaponList.add(spellbookAdapter);
            }
        }
        return weaponList;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        if(attackType == 0){
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (normal attack): " + weapon.normalAttack());
        }
        else{
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (charged attack): " + weapon.chargedAttack());
        }
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
