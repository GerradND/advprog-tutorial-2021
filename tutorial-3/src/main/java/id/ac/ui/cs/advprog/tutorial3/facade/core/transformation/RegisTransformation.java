package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class RegisTransformation implements Transformation{
    private int key;

    public RegisTransformation(int key){
        this.key = key;
    }

    public RegisTransformation(){
        this(13);
    }

    @Override
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    @Override
    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        char[] myChar = text.toCharArray();
        for (int i = 0; i < myChar.length; i++) {
            char letter = myChar[i];

            if (letter >= 'a' && letter <= 'z') {
                // Rotate lowercase letters.

                if (letter > 'm') {
                    letter -= key;
                } else {
                    letter += key;
                }
            } else if (letter >= 'A' && letter <= 'Z') {
                // Rotate uppercase letters.

                if (letter > 'M') {
                    letter -= key;
                } else {
                    letter += key;
                }
            }
            res[i] = letter;
        }
        // Convert array to a new String.
        return new Spell(new String(res), codex);
    }
}
