package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {
    private Bow bow;
    private Boolean isAimShot;

    public BowAdapter(Bow bow){
        if(bow.getName().equalsIgnoreCase("Ionic Bow")){
            this.bow = new IonicBow(bow.getHolderName());
        }
        else{
            this.bow = new UranosBow(bow.getHolderName());
        }
        isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        isAimShot = !(isAimShot);
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
