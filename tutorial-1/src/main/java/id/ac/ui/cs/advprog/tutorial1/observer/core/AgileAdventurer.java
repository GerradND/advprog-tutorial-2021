package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        // Menambahkan AgileAdventurer ke dalam guild
        guild.add(this);
    }

    @Override
    public void update() {
        // Mengupdate quest untuk AgileAdventurer sesuai jenis quest
        String questType = this.guild.getQuestType();
        if (questType.equals("D")){
            this.getQuests().add(this.guild.getQuest());
        }
        if (questType.equals("R")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }

    //ToDo: Complete Me
}
