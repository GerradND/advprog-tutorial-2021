package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "ddu-du ddu-du";
    }

    @Override
    public String getType() {
        return "Attack with gun";
    }
}
