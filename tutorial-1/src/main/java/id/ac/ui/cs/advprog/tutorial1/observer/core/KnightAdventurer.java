package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        guild.add(this);
    }

    @Override
    public void update() {
        // Mengupdate quest untuk KnightAdventurer sesuai jenis quest
        this.getQuests().add(this.guild.getQuest());
    }
}
