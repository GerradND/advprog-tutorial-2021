package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "Anti tembus-tembus club";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
